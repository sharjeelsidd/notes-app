const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) =>{
    const notes = loadNotes()
    // const duplicateNotes = notes.filter((note) => note.title === title)
    const duplicateNote = notes.find((note) => note.title === title)
    if(!duplicateNote){
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse("New notes added!"))
    }else{
        console.log(chalk.red.inverse("Notes title taken!"))
    }
}
const removeNote = (title) => {
    const notes = loadNotes()
    const matchingNote = notes.filter((note) => note.title !== title) 
    if(notes.length > matchingNote.length){
        console.log(chalk.green.inverse("Notes was removed"))
        saveNotes(matchingNote)
    }else{
        console.log(chalk.red.inverse("Note with that name doesn't exist."))
    }
    
}

const saveNotes = (notes) => {
    const save = JSON.stringify(notes)
    fs.writeFileSync('notes.json', save)
}

// Code below loads the notes. 

const loadNotes = () =>  {
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
     }  catch(e){
           return []
        }
    }   

const listNotes = () => {
    const notes = loadNotes()
    notes.forEach((notes) =>{
        console.log(notes.title)
    })
}   

const readNote = (title) => {
    const notes = loadNotes()
    const note = notes.find((note) => note.title === title)
    if(note){
        console.log(chalk.inverse(note.title))
        console.log(note.body)
    }
    else{
        console.log(chalk.red.red('No notes found'))
    } 
}
    
module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}